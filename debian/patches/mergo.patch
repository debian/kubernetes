Last-Update: 2017-11-07
Forwarded: no
Bug-Upstream: https://github.com/kubernetes/kubernetes/issues/27543
Bug-Debian: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=825753
Bug-Debian: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=878254
Author: Dmitry Smirnov <onlyjob@member.fsf.org>
Description: update for "github.com/imdario/mergo" v0.2.2.

--- a/staging/src/k8s.io/client-go/tools/clientcmd/client_config.go
+++ b/staging/src/k8s.io/client-go/tools/clientcmd/client_config.go
@@ -168,15 +168,15 @@
 		userAuthPartialConfig, err := config.getUserIdentificationPartialConfig(configAuthInfo, config.fallbackReader, persister)
 		if err != nil {
 			return nil, err
 		}
-		mergo.Merge(clientConfig, userAuthPartialConfig)
+		mergo.MergeWithOverwrite(clientConfig, userAuthPartialConfig)
 
 		serverAuthPartialConfig, err := getServerIdentificationPartialConfig(configAuthInfo, configClusterInfo)
 		if err != nil {
 			return nil, err
 		}
-		mergo.Merge(clientConfig, serverAuthPartialConfig)
+		mergo.MergeWithOverwrite(clientConfig, serverAuthPartialConfig)
 	}
 
 	return clientConfig, nil
 }
@@ -194,9 +194,9 @@
 	configClientConfig := &restclient.Config{}
 	configClientConfig.CAFile = configClusterInfo.CertificateAuthority
 	configClientConfig.CAData = configClusterInfo.CertificateAuthorityData
 	configClientConfig.Insecure = configClusterInfo.InsecureSkipTLSVerify
-	mergo.Merge(mergedConfig, configClientConfig)
+	mergo.MergeWithOverwrite(mergedConfig, configClientConfig)
 
 	return mergedConfig, nil
 }
 
@@ -256,10 +256,10 @@
 		}
 		promptedConfig := makeUserIdentificationConfig(*promptedAuthInfo)
 		previouslyMergedConfig := mergedConfig
 		mergedConfig = &restclient.Config{}
-		mergo.Merge(mergedConfig, promptedConfig)
-		mergo.Merge(mergedConfig, previouslyMergedConfig)
+		mergo.MergeWithOverwrite(mergedConfig, promptedConfig)
+		mergo.MergeWithOverwrite(mergedConfig, previouslyMergedConfig)
 		config.promptedCredentials.username = mergedConfig.Username
 		config.promptedCredentials.password = mergedConfig.Password
 	}
 
@@ -399,13 +399,13 @@
 	contextName, required := config.getContextName()
 
 	mergedContext := clientcmdapi.NewContext()
 	if configContext, exists := contexts[contextName]; exists {
-		mergo.Merge(mergedContext, configContext)
+		mergo.MergeWithOverwrite(mergedContext, configContext)
 	} else if required {
 		return clientcmdapi.Context{}, fmt.Errorf("context %q does not exist", contextName)
 	}
-	mergo.Merge(mergedContext, config.overrides.Context)
+	mergo.MergeWithOverwrite(mergedContext, config.overrides.Context)
 
 	return *mergedContext, nil
 }
 
@@ -415,13 +415,13 @@
 	authInfoName, required := config.getAuthInfoName()
 
 	mergedAuthInfo := clientcmdapi.NewAuthInfo()
 	if configAuthInfo, exists := authInfos[authInfoName]; exists {
-		mergo.Merge(mergedAuthInfo, configAuthInfo)
+		mergo.MergeWithOverwrite(mergedAuthInfo, configAuthInfo)
 	} else if required {
 		return clientcmdapi.AuthInfo{}, fmt.Errorf("auth info %q does not exist", authInfoName)
 	}
-	mergo.Merge(mergedAuthInfo, config.overrides.AuthInfo)
+	mergo.MergeWithOverwrite(mergedAuthInfo, config.overrides.AuthInfo)
 
 	return *mergedAuthInfo, nil
 }
 
@@ -430,15 +430,15 @@
 	clusterInfos := config.config.Clusters
 	clusterInfoName, required := config.getClusterName()
 
 	mergedClusterInfo := clientcmdapi.NewCluster()
-	mergo.Merge(mergedClusterInfo, config.overrides.ClusterDefaults)
+	mergo.MergeWithOverwrite(mergedClusterInfo, config.overrides.ClusterDefaults)
 	if configClusterInfo, exists := clusterInfos[clusterInfoName]; exists {
-		mergo.Merge(mergedClusterInfo, configClusterInfo)
+		mergo.MergeWithOverwrite(mergedClusterInfo, configClusterInfo)
 	} else if required {
 		return clientcmdapi.Cluster{}, fmt.Errorf("cluster %q does not exist", clusterInfoName)
 	}
-	mergo.Merge(mergedClusterInfo, config.overrides.ClusterInfo)
+	mergo.MergeWithOverwrite(mergedClusterInfo, config.overrides.ClusterInfo)
 	// An override of --insecure-skip-tls-verify=true and no accompanying CA/CA data should clear already-set CA/CA data
 	// otherwise, a kubeconfig containing a CA reference would return an error that "CA and insecure-skip-tls-verify couldn't both be set"
 	caLen := len(config.overrides.ClusterInfo.CertificateAuthority)
 	caDataLen := len(config.overrides.ClusterInfo.CertificateAuthorityData)
--- a/staging/src/k8s.io/client-go/tools/clientcmd/loader.go
+++ b/staging/src/k8s.io/client-go/tools/clientcmd/loader.go
@@ -208,24 +208,24 @@
 	// first merge all of our maps
 	mapConfig := clientcmdapi.NewConfig()
 
 	for _, kubeconfig := range kubeconfigs {
-		mergo.Merge(mapConfig, kubeconfig)
+		mergo.MergeWithOverwrite(mapConfig, kubeconfig)
 	}
 
 	// merge all of the struct values in the reverse order so that priority is given correctly
 	// errors are not added to the list the second time
 	nonMapConfig := clientcmdapi.NewConfig()
 	for i := len(kubeconfigs) - 1; i >= 0; i-- {
 		kubeconfig := kubeconfigs[i]
-		mergo.Merge(nonMapConfig, kubeconfig)
+		mergo.MergeWithOverwrite(nonMapConfig, kubeconfig)
 	}
 
 	// since values are overwritten, but maps values are not, we can merge the non-map config on top of the map config and
 	// get the values we expect.
 	config := clientcmdapi.NewConfig()
-	mergo.Merge(config, mapConfig)
-	mergo.Merge(config, nonMapConfig)
+	mergo.MergeWithOverwrite(config, mapConfig)
+	mergo.MergeWithOverwrite(config, nonMapConfig)
 
 	if rules.ResolvePaths() {
 		if err := ResolveLocalPaths(config); err != nil {
 			errlist = append(errlist, err)
--- a/staging/src/k8s.io/client-go/tools/clientcmd/client_config_test.go
+++ b/staging/src/k8s.io/client-go/tools/clientcmd/client_config_test.go
@@ -33,17 +33,17 @@
 		X string
 	}
 	dst := T{X: "one"}
 	src := T{X: "two"}
-	mergo.Merge(&dst, &src)
+	mergo.MergeWithOverwrite(&dst, &src)
 	if dst.X != "two" {
-		// mergo.Merge changed in an incompatible way with
+		// mergo.MergeWithOverwrite changed in an incompatible way with
 		//
 		//   https://github.com/imdario/mergo/commit/d304790b2ed594794496464fadd89d2bb266600a
 		//
 		// We have to stay with the old version which still does eager
 		// copying from src to dst in structs.
-		t.Errorf("mergo.Merge library found with incompatible, new behavior")
+		t.Errorf("mergo.MergeWithOverwrite library found with incompatible, new behavior")
 	}
 }
 
 func createValidTestConfig() *clientcmdapi.Config {
